Source: kpipewire
Section: libs
Priority: optional
Maintainer: Jonathan Esk-Riddell <jr@jriddell.org>
Build-Depends: cmake (>= 3.16~),
               debhelper-compat (= 13),
               kf6-extra-cmake-modules,
               kf6-kcoreaddons-dev,
               kf6-ki18n-dev,
               kwayland-dev,
               libavcodec-dev,
               libavfilter-dev,
               libavformat-dev,
               libavutil-dev,
               libdrm-dev (>= 2.4.62~),
               libegl1-mesa-dev,
               libepoxy-dev (>= 1.3~),
               libgbm-dev,
               libpipewire-0.3-dev,
               libswscale-dev,
               libva-dev,
               libwayland-dev,
               pkg-kde-tools-neon,
               plasma-wayland-protocols,
               qt6-base-dev,
               qt6-declarative-dev,
               qt6-wayland-dev,
               qt6-wayland-dev-tools
Standards-Version: 4.6.0
Homepage: https://invent.kde.org/plasma/kpipewire
Vcs-Browser: https://invent.kde.org/neon/neon-packaging/kpipewire
Vcs-Git: https://invent.kde.org/neon/neon-packaging/kpipewire.git
Rules-Requires-Root: no

Package: libkpipewire-dev
Section: libdevel
Architecture: any
Depends: libkpipewire6 (= ${binary:Version}),
         libkpipewirerecord6 (= ${binary:Version}),
         libpipewire-0.3-dev,
         qt6-base-dev,
         ${misc:Depends}
Breaks: libkpipewire5 (<< 5.27.9)
Replaces: libkpipewire5 (<< 5.27.9)
Description: KDE's Pipewire library
 Components for rendering and recording PipeWire streams using Qt.
 .
 This package contains the library development files.

Package: libkpipewire6
Architecture: any
Depends: qt6-base, ${misc:Depends}, ${shlibs:Depends}
Breaks: libkpipewire5 (<< 6.0.1)
Replaces: libkpipewire5 (<< 6.0.1)
Description: KDE's Pipewire library
 Components for rendering and recording PipeWire streams using Qt.
 .
 This package contains the kpipewire main library.

Package: libkpipewirerecord6
Architecture: any
Depends: qt6-base, ${misc:Depends}, ${shlibs:Depends}
Breaks: libkpipewirerecord5 (<< 6.0.1)
Replaces: libkpipewirerecord5 (<< 6.0.1)
Description: KDE's Pipewire library
 Components for rendering and recording PipeWire streams using Qt.
 .
 This package contains the kpipewirerecord library.

Package: qml6-module-org-kde-pipewire
Architecture: any
Depends: libkpipewire6 (= ${binary:Version}),
         libkpipewirerecord6 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: qml-module-org-kde-pipewire (<< 6.0.1)
Replaces: qml-module-org-kde-pipewire (<< 6.0.1)
Description: KDE's Pipewire library
 Components for rendering and recording PipeWire streams using Qt.
 .
 This package contains the QtQuick module.

Package: libkpipewire5
Architecture: all
Depends: libkpipewire6, ${misc:Depends}
Section: oldlibs
Priority: optional
Description: transitional package
 This is a dummy package which can be removed.

Package: libkpipewiredmabuf5
Architecture: all
Depends: libkpipewire6, ${misc:Depends}
Section: oldlibs
Priority: optional
Description: transitional package
 This is a dummy package which can be removed.

Package: libkpipewiredmabuf6
Architecture: all
Depends: libkpipewire6, ${misc:Depends},
Breaks: libkpipewiredmabuf5 (<< 6.0.1)
Replaces: libkpipewiredmabuf5 (<< 6.0.1)
Section: oldlibs
Priority: optional
Description: transitional package
 This is a dummy package which can be removed.

Package: libkpipewirerecord5
Architecture: all
Depends: libkpipewirerecord6, ${misc:Depends}
Section: oldlibs
Priority: optional
Description: transitional package
 This is a dummy package which can be removed.

Package: qml-module-org-kde-pipewire
Architecture: all
Depends: qml6-module-org-kde-pipewire, ${misc:Depends}
Section: oldlibs
Priority: optional
Description: transitional package
 This is a dummy package which can be removed.
